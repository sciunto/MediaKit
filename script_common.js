
/****************************************************************************/
/* (C) Benjamin Sonntag 2010, distributed under GPL v3  License.

/* Supprime le composant s qui doit �tre enfant du composant t. */
function remove(t,s) {
  if (c=document.getElementById(t)) {
    for (i=0;i<c.childNodes.length;i++) {
      if (c.childNodes[i].id==s) {
        c.removeChild(c.childNodes[i]);
        return true;
      }
    }
  }
  return false;
}


/* Returns a new xmlhttprequest object. */
function getXmlObject() {
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
   try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
   } catch (e) {
    try {
     xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
     xmlhttp = false;
   }
   }
@end @*/
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}


/* Cache le composant s */
  function hide(s) {
     if (document.all) {
       if (document.all[s]) {
         document.all[s].visibility="invisible";
         eval("document.all."+s+".style.display=\"none\"");
       }
     } else {
       if (document.getElementById(s)) {
         document.getElementById(s).visibility="invisible";
         document.getElementById(s).style.display="none";
       }
     }
   }

/* Affiche le composant s */
 function show(s,shm) {
   if (!shm) shm="block";
   if (document.all) {
     if (document.all[s]) {
       document.all[s].visibility="visible";
       eval("document.all."+s+".style.display=\""+shm+"\"");
     }
   } else {
     if (document.getElementById(s)) {
       document.getElementById(s).visibility="visible";
       document.getElementById(s).style.display=shm;
     }
   }
 }

/* Affiche / Cache le composant s */
function swap(s,shm) {
   if (document.all) {
     if (document.all[s]) {
       if (document.all[s].visibility=="visible") {
       	 hide(s);
       } else {
         show(s,shm);
       }
     }
   } else {
     if (document.getElementById(s)) {
       if (document.getElementById(s).visibility=="visible") {
       	 hide(s);
       } else {
         show(s,shm);
       }
     }
   }
}

var swap1_component=0;
/* Affiche / Cache le composant s.
   Si swap1 a d�j� �t� appel� pour un composant, l'affichant, on 
   cache ce premier composant.
   si s vaut 0, cache le composant affich� pr�alablement.
 */
function swap1(s,shm) {
   if (!s && swap1_component) {
	hide(swap1_component);
	swap1_component=0;
   }
   if (document.all) {
     if (document.all[s]) {
       if (document.all[s].visibility=="visible") {
       	 hide(s);
	swap1_component=0;
       } else {
	if (swap1_component) hide(swap1_component);
         show(s,shm);
	swap1_component=s;
       }
     }
   } else {
     if (document.getElementById(s)) {
       if (document.getElementById(s).visibility=="visible") {
       	 hide(s);
	swap1_component=0;
       } else {
	if (swap1_component) hide(swap1_component);
         show(s,shm);
	swap1_component=s;
       }
     }
   }
}


/* Donne la position en X du composant obj */
function findPosX(obj) {
	var curleft = 0;
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

/* Donne la position en Y du composant obj */
function findPosY(obj) {
	var curtop = 0;
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	return curtop;
}

/* D�place le composant s � tx/ty pixels du composant t */
function moveNear(s,t,tx,ty) {
	if ((ss=document.getElementById(s)) && (tt=document.getElementById(t))) {
		ss.style.left=(findPosX(tt)+tx)+"px";
		ss.style.top=(findPosY(tt)+ty)+"px";
	}
}

function seekToTime(value)
{
//    var seekToTime = parseInt($('#example_video_1').attr('currentTime')) + value;
    var seekToTime = parseInt(value);
    if( seekToTime < 0 || seekToTime > $('#example_video_1').attr('duration') ) 
        return;
    $('#example_video_1').attr('currentTime',seekToTime);
}
