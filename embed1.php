<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" dir="ltr">
<head>
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <meta http-equiv="Content-Type" content="text/html; charset=ISO8859-1" />
  <script type="text/javascript" src="/swfobject.js"></script>
  <script type="text/javascript" src="/component_script.js"></script>
  <script type="text/javascript" src="/script_common.js"></script>

  <script src="/video.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    VideoJS.setupAllWhenReady();
  </script>

  <link rel="stylesheet" href="/video-js.css" type="text/css" media="screen" title="Video JS">
  <link rel="stylesheet" type="text/css" href="/style.css" />
  <link rel="stylesheet" type="text/css" href="/component_style.css" />
</head>
   <body class="ifr<?php if (isset($_REQUEST["onlyvideo"])) { echo "2"; } ?>">
<?php

require_once("config.php");

/*
if ($_GET["id"]==716) {
  echo '<iframe width="560" height="315" src="http://www.youtube.com/embed/citzRjwk-sQ" frameborder="0" style="padding-left: 80px; padding-top: 80px" allowfullscreen></iframe>';
  exit();
}

$_GET["size"]="small";
*/

if ($id=intval($_REQUEST["id"])) {
  $m=mqone("SELECT * FROM media WHERE id='$id';");

  if (isset($_REQUEST["help"])) {
    ?>
<h3>How To embed this media?</h3>
<table><tr><th>Title: </th><td><?php echo $m["title"]; ?></td></tr>
<tr><th>Description:</th><td><?php echo $m["description"]; ?></td></tr>
</table>

													  <p>&nbsp;</p>
      <p>Make a link to <a href="http://mediakit.laquadrature.net/embed/<?php echo $id; ?>">http://mediakit.laquadrature.net/embed/<?php echo $id; ?></a></p>
      <p>You can add "?" parameters to this link as follow : </p>
<ul>
      <li><code>?help</code> to obtain this help page</li>
      <li><code>?size=big</code> to obtain the 1024x768 (16:9) or 1024x576 (4:3) video at 2Mbps</li>
      <li><code>?size=medium</code> to obtain the 640x360 (16:9) or 640x480 (4:3) video at 2Mbps</li>
      <li><code>?size=small</code> to obtain the 480x272 (16:9) or 432x320 (4:3) video at 500Kbps</li>
      <li><code>?onlyvideo</code> to get only the video (no title above, description below, and no download links)</li>
      <li><code>?sub=fr_FR</code> ask for the hard-coded subtitled version for French (fr_FR) you can use any language that is available</li>
														       
</ul>
       <p>If you mix them, separate them by &amp; instead of ? like this : http://mediakit.laquadrature.net/embed/<?php echo $id; ?>?size=big&onlyvideo</p>
</body>
</html>
<?php
    exit();
  }

  switch ($m["type"]) {


  case MEDIA_IMAGE:
    if (($m["vx"]<=250) && ($m["vy"]<=250)) {
      echo "<img src=\"files/".$m["filename"]."\" style=\"width: ".$m["vx"]."px; height: ".$m["vy"]."px;\"/>";
    } else {
      $ok=mqone("SELECT * FROM mediaformat WHERE media='$id' AND format='8';");
      if ($ok["size"]!=0) {
	echo "<img src=\"formats/8/$id\" />";
      } else {
	if ($m["vx"]>$m["vy"]) { 
	  $x=250; $y=$m["vy"]/($m["vx"]/250);
	} else {
	  $y=250; $x=$m["vx"]/($m["vy"]/250);
	}
	echo "<img src=\"files/".$m["filename"]."\" style=\"width: ".$x."px; height: ".$y."px;\"/>";
      }
    }
    break;



  case MEDIA_VIDEO:
    // 2 cases: all .mp4 & others has been computed (new mode) 
    // or not (old mode) 
    if (
	//	file_exists("formats/18/".$id."_big.ogg") && 
	//	file_exists("formats/18/".$id."_small.ogg") && 
	file_exists("formats/19/".$id."_big.webm") && 
	file_exists("formats/19/".$id."_small.webm") && 
	file_exists("formats/20/".$id."_big.mp4") && 
	file_exists("formats/20/".$id."_small.mp4") ) {
      // NEW !
      if (!$_GET["size"]) { $_GET["size"]="medium"; }
      require("view2.php");


    }  else {
      // OLD ! 
      if ($_REQUEST["tc"]) $add="_tc".doubleval($_REQUEST["tc"]);
      if (isset($_REQUEST["autostart"])) {
	if ($_REQUEST["autostart"]) {
	  $autostart="true";
	} else {
	  $autostart="false";
	}
      } else {
	$autostart="true";
      }
      if (isset($_REQUEST["width"])) {
	$x=intval($_REQUEST["width"]);
	$y=intval($x/1.25+32);
      } else {
	$x=360;
	$y=320;
      }
      ?>
    <div id="container"><a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.</div>
    <script type="text/javascript">
    var s1 = new SWFObject("player.swf","ply","<?php echo $x; ?>","<?php echo $y; ?>","9","#FFFFFF");
  s1.addParam("allowfullscreen","true");
  s1.addParam("allownetworking","all");
  s1.addParam("allowscriptaccess","always");
  s1.addParam("flashvars","file=<?php echo urlencode("http://".$_SERVER["HTTP_HOST"]."/formats/12/".$id.$add.".flv"); ?>&autostart=<?php echo $autostart; ?>");
  s1.write("container");
	      </script>
<?php	      
	      if (!$_REQUEST["short"]) {
		echo "<p>lien direct du fichier video : <a href=\"http://".$_SERVER["HTTP_HOST"]."/formats/12/".$id.$add.".flv\">Format FLV</a> - ";
		echo "<a href=\"http://".$_SERVER["HTTP_HOST"]."/formats/13/".$id.$add.".ogv\">Format OGV</a></p>";
		echo "<p><a href=\"/\">Retour au mediakit de La Quadrature du Net</a></p>";
	      }
    }
    break;


  case MEDIA_AUDIO:
?>
        <div id="container"><a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.</div>

        <script type="text/javascript">
	var s1 = new SWFObject("player.swf","ply","360","20","9","#FFFFFF");
s1.addParam("allowfullscreen","true");
s1.addParam("allownetworking","all");
s1.addParam("allowscriptaccess","always");
s1.addParam('wmode','opaque');
s1.addParam("flashvars","file=<?php echo urlencode("http://".$_SERVER["HTTP_HOST"]."/formats/10/".$id.".mp3"); ?>&autostart=true");
s1.write("container");
        </script>
	      
<?php
    break;


  }
    } else {
  echo "Media not found. ";
 }


?>
</body>
</html>
