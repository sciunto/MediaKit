

/****************************************************************************/
/* (C) Benjamin Sonntag 2010, distributed under GPL v3  License.

/* multiselect functions */


/* Fonction appell�e quand on veut supprimer un �lement */
function ms_del(name,el) {
  remove(name+'d',name+'v_'+el);
  kl=document.getElementById(name+'l'); // document.forms['f1'].klist;
  if (kl) kl.focus();
}


/* Fonction appell�e quand on veut ajouter un �l�ment */
function ms_add(name) {
  kl=document.getElementById(name+'l'); // document.forms['f1'].klist;
  di=document.getElementById(name+'d');
  if (kl && di) {
    i=kl.selectedIndex;
    k=kl.options[i].text;
    v=kl.options[i].value;
    if (!(c=document.getElementById(name+'v_'+v))) {
      di.innerHTML=di.innerHTML+'<div id="'+name+'v_'+v+'"><input type="hidden" name="'+name+'v[]" id="" value="'+v+'"/><a href="javascript:ms_del(\''+name+'\','+v+')"><img src="del.gif"></a>&nbsp;'+k+' &nbsp; </div>';
    }
    kl.focus();
  }
}


/* Fonction appell�e quand on appuie une touche sur la liste d�roulante */
function ms_key(name,evt) {
  var keyCode = evt.which ? evt.which : evt.keyCode;
  if (keyCode==13) {
    ms_add(name);
    return false;
  }
  return true;
}


/****************************************************************************/
/* checkDB  functions */

/* Check a text line whose id is 'va' from the database.
   the component set is comp+'h' for hidden value, comp+'i' for the check image
   fun is the page we shall call with ajax to check this value.
   this page prototype is : $_REQUEST[value]=the value, returns an id (>0) or 0 if
   it cannot find it.
*/
var cdbcheck_x,cdbcheck_h,cdbcheck_i;
var cdb_to;

function cdb_check(comp,va,fun) {
	cdbcheck_x=getXmlObject();
	cdbcheck_h=document.getElementById(comp+'h');
	cdbcheck_i=document.getElementById(comp+'i');
	cdbcheck_v=document.getElementById(va);
	if (cdbcheck_x && cdbcheck_h && cdbcheck_i && cdbcheck_v) {
		cdbcheck_h.value=0;
		cdbcheck_i.src="list_unchecked.gif";
		cdbcheck_x.open("GET", fun+"?value="+cdbcheck_v.value,true);
		cdbcheck_x.onreadystatechange=function() {
			if (cdbcheck_x.readyState==4) {
				t=cdbcheck_x.responseText;
				if (t!=0) {
					cdbcheck_h.value=t;
					cdbcheck_i.src="list_good.gif";
				} else {
					cdbcheck_h.value=0;
					cdbcheck_i.src="list_bad.gif";
				}
	    		}
	  	}
		cdbcheck_x.send(null);
	} else {
	alert("canceled "+cdbcheck_x+"/"+cdbcheck_h+"/"+cdbcheck_i);
	}
}

/* Appeler cette fonction quand le composant texte � checker change : */
function cdb_keyup(comp,va,fun) {
	// TODO : make an automatic change after a time ...
	cdbcheck_v=document.getElementById(va);
	cdbcheck_h=document.getElementById(comp+'h');
        cdbcheck_i=document.getElementById(comp+'i');
	if (cdbcheck_v) {
		if (cdbcheck_v.value!=cdbcheck_v.oldval) {
			window.status='val:'+cdbcheck_v.value+' old:'+cdbcheck_v.oldval;
			if (cdb_to) {
				window.clearTimeout(cdb_to);
			}
			cdb_to=window.setTimeout("cdb_check('"+comp+"','"+va+"','"+fun+"')",1000);
			cdbcheck_h.value=0;
			cdbcheck_i.src="list_unchecked.gif";
			cdbcheck_v.oldval=cdbcheck_v.value;
		}
	}
	return true;
}


/****************************************************************************/
/* editTable functions */

var et_isEditing=false;

/* Remplace le composant [cellid] de largeur [tx] pixels par un formulaire 
   permettant de modifier la valeur de ce composant. 
   Stocke l'ancienne valeur pour pouvoir la remettre si besoin.
*/
function editCell(cellid,tx) {
	if (cellid==et_isEditing) return true;
	if (et_isEditing) {
		cancelEditCell(et_isEditing);
	}
	et_isEditing=cellid;
	if (c=document.getElementById(cellid)) {
	c.oldval=c.innerHTML;
	c.innerHTML='<form method="get" action="" id="f'+cellid+'"><input type="text" name="edit" value="'+c.oldval+'" style="width: '+tx+'px" onkeyDown="return editCellKey(event);" onBlur="cancelEditCell()"/></form>';
	document.forms['f'+cellid].edit.focus();
	}
}

/* Annule l'�dition d'un composant. Remet son ancienne valeur. */
function cancelEditCell() {
	if (c=document.getElementById(et_isEditing)) {
		c.innerHTML=c.oldval;
	}
}

/* Fonction appell�e quand on appuie une touche sur le formulaire de 
   modification de la valeur d'un composant. D�tourne ECHAP et ENTER
*/
function editCellKey(evt) {
  var keyCode = evt.which ? evt.which : evt.keyCode;
  var y;
  if (keyCode==27 && et_isEditing) {
	cancelEditCell();
	return true;
  }
  if ((keyCode==13 && et_isEditing) || (keyCode==9 && et_isEditing)) { // Applique la nouvelle valeur.
	if (c=document.getElementById(et_isEditing)) {
		va=document.forms['f'+et_isEditing].edit.value;
		c.innerHTML=va;
		if (c.oldval!=va) {
			y=et_isEditing.split(/[a-zA-Z]+/);	
			// here y[1]= table number, y[2]= id of the object y[3]= column number
			// parameters are table-number, row-number, column-alias, new-value
			if (y[3]) {
				updateData(y[1],y[2],listTableAlias[y[1]][y[3]],va);
			} else {
				updateData(y[0],y[1],listTableAlias[y[0]][y[2]],va);
			}
		}
		et_isEditing=false;
	}
	return false;
  }
  return true;
}

var listTableAlias=Array();


/* Fonctions de callback : 
   DOIVENT �tre modifi�es pour faire quelque chose, g�n�ralement en Javascript
*/

/* Active / Inactive la colonne d'alias t */
function swapColumn(t) {
	alert("Swapping Column "+t);
}

/* Change le mode de tri pour t */
function changeSort(t) {
	alert("Changing sort mode for column "+t);
}

/* Met � jour une information en ligne */
/*
function updateData(tbl,row,col,val) {
	alert("updating data :\nTable:"+tbl+"\nRow:"+row+"\nCol:"+col+"\nNewVal:"+val);
}
*/


/* / listTable functions */
/****************************************************************************/

