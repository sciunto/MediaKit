<?php

if (!$_SERVER["REMOTE_USER"]) {
  echo "Not Allowed";
  exit();
 }

require_once("config.php");

$id=intval($_REQUEST["id"]);

$m=mqone("SELECT * FROM media WHERE id='$id';");

if (!$id || !$m) {
  $error[]="ID de media non fourni ou media non trouvé !";
  require_once("index.php");
  exit();
 }

if ($_REQUEST["go"]) {
  // On modifie le media
  $filename=str_replace("/","",$_REQUEST["filename"]);
  $title=$_REQUEST["title"];
  if ($filename!=$m["filename"]) {
    if (file_exists("files/$filename")) {
      $error[]="Le nom de fichier voulu existe déjà, le media n'a donc pas été renommé";
      $filename=$m["filename"];
    } else {
      rename("files/".sts($m["filename"]),"files/".sts($filename));
    }
  }
  mq("UPDATE media SET filename='".asl($filename)."', title='".asl($title)."', description='".asl($_REQUEST["description"])."', datec='".asl($_REQUEST["datec"])."' WHERE id='$id';");
  // Now the tags ...
  mq("DELETE FROM mediatag WHERE media='$id';");
  $groups=mqlistone("SELECT id FROM taggroup;");
  $private=0;
  foreach($groups as $gid) {
    if (is_array($_REQUEST["tg".$gid."v"])) {
      foreach($_REQUEST["tg".$gid."v"] as $tagid) {
	mq("INSERT INTO mediatag SET media='$id',tag='$tagid';");
	if (in_array($tagid,$privatetags)) {
	  $private=1;
	}
      }
    }
  }
  mq("UPDATE media SET private='$private' WHERE id='$id';");
  $info[]="Media modifié";
  require_once("index.php");
  exit();
 } else {
  $fields=array("filename","description","datec","title");
  foreach($fields as $f) $_REQUEST[$f]=nasl($m[$f]);
 }

require_once("head.php");

?>

<iframe id="view" name="view" style="position: fixed; top: 30px; right: 30px; width: 500px; height: 450px; border: 2px inset blue; padding: 2px" src="view.php?id=<?=$id; ?>&size=small"></iframe>

<h2>Edition du media <?=$m["filename"]; ?></h2>

<form method="post" action="edit.php">
  <input type="hidden" name="id" value="<?=$id; ?>"/>
<table class="formh">
<tr>
 <th>Type de media</th>
  <td><b><?php echo $amedia[$m["type"]]; ?></b></td>
</tr>
<tr>
 <th>Nom du fichier</th>
 <td><input type="text" name="filename" size="60" value="<? eher("filename"); ?>" /></td>
</tr>
<tr>
 <th>Titre fichier</th>
 <td><input type="text" name="title" size="60" value="<? eher("title"); ?>" /></td>
</tr>
<tr>
 <th colspan="2">Description</th>
</tr><tr>
 <td colspan="2"><textarea cols="80" rows="6" name="description"><? eher("description"); ?></textarea></td>
</tr>
<tr>
 <th>Date (aaaa-mm-jj hh:ii:ss)</th>
 <td><input type="text" name="datec" value="<? eher("datec"); ?>" /></td>
</tr>
<tr>
 <th>Tags associés au media</th>
 <td>
   <table>
   <?php
  
  function tagoption($gid) {
  $r=mq("SELECT id,name FROM tag WHERE groupid='$gid';");
  while ($c=mysql_fetch_array($r)) {
    echo "<option value=\"".$c["id"]."\">".$c["name"]."</option>";
  }
}

  $r=mq("SELECT mediatag.tag, tag.groupid, tag.name FROM mediatag, tag WHERE mediatag.media='$id' AND mediatag.tag=tag.id ORDER BY tag.groupid,tag.name;");
echo mysql_error();
  while ($c=mysql_fetch_array($r)) {
    $curtags[$c["groupid"]][$c["tag"]]=$c["name"];
  }
  $groups=mqassoc("SELECT id,name FROM taggroup ORDER BY name;");
  require_once("component_multiselect.php");

foreach($groups as $gid=>$gname) {
  echo "<tr><th>$gname</th>";
?>
<td valign="top" style="vertical-align: top">
<select class="inl" name="tg<?=$gid ?>l" id="tg<?=$gid ?>l" onKeyDown="return ms_key('tg<?=$gid ?>',event);">
   <?php tagoption($gid); ?>
      </select>&nbsp;&nbsp;<a href="javascript:ms_add('tg<?=$gid ?>')"><img src="add.gif" style="vertical-align: middle" alt="Ajouter ce tag" title="Ajouter ce tag" /></a>
</td>
<td>
<div id="tg<?=$gid ?>d">
<?php
			      ms_divFromArray($curtags[$gid],"tg".$gid);
?>
</div>
<?php
  echo "</td></tr>\n";
}

   ?>
   </table>
   </td>
</tr>
<tr><td colspan="2">
  <small>Les tags INA et PRIVÉ ont pour conséquence que la vidéo ne sera pas affichée sur le site public</small>
</td></tr>
<tr><td colspan="2" style="text-align: right">
 <input type="submit" name="go" value="Modifier" />
 <input type="button" name="cancel" value="Annuler" onclick="document.location='/';" />
</td></tr>
</table>
</form>


<?php
  require_once("foot.php");
?>