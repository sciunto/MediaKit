<?php

require_once("config.php");

require_once("head.php");

if (!isset($_REQUEST["type"])) $_REQUEST["type"]=-1;
if (!isset($_REQUEST["count"])) $_REQUEST["count"]=100;
if (!isset($_REQUEST["show"])) $_REQUEST["show"]=2;

?>
<form method="get" action="/" id="f1" name="f1">
<table>
  <tr><th>Filtrage : </th>
<td><select name="type" onchange="document.forms['f1'].submit()">
  <?php $atype=array(-1=>"Tous les types",
		     MEDIA_UNKNOWN=>"Inconnus",
		     MEDIA_AUDIO=>"Audio",
		     MEDIA_VIDEO=>"Vidéo",
		     MEDIA_IMAGE=>"Images",
		     );
eoption($atype,$_REQUEST["type"]);
		     ?></select>
</td>
<td>
		     Recherche <input type="text" name="q" id="q" value="<?php eher("q"); ?>"/> <input type="submit" name="go" id="go" value="Filtrer" />
</td>
</tr>
  <tr><th>Affichage : </th>
<td><select name="show" onchange="document.forms['f1'].submit()">
  <?php $ashow=array(0=>"Nom de fichiers seuls",
		     1=>"Nom, description",
		     2=>"Nom, description, tags",
		     );
eoption($ashow,$_REQUEST["show"]);
		     ?></select>
</td>
<td>
<select name="count" onchange="document.forms['f1'].submit()">
  <?php $acount=array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000);
eoption($acount,$_REQUEST["count"]); ?>
</select>&nbsp;par page&nbsp; 
</td>
<td>
<?php 

if ($_SERVER["REMOTE_USER"]) {
?>
<a href="tags.php">Gérer les tags</a>
    <?php } ?>
</td>
</tr>
</table>
</form>

<div  style="position: fixed; top: 30px; right: 30px; border: 2px inset blue; padding: 3px;" >
<p style="padding:0;margin:0"><a href="javascript:swapmonitor();" alt="Affiche / Masque le moniteur (alt-shift-h)" title="Affiche / Masque le moniteur (alt-shift-h)" accesskey="h">+/- moniteur</a></p>
<iframe id="view" name="view" style="width: 680px; height: 540px; border: 0px; margin: 0; padding: 0; <?php 
if ($_COOKIE['showmonitor']==0) {
echo "display: none;";
}
?>" src="view.php?id=0"></iframe>
<script type="text/javascript">
 <?php if ($_COOKIE['showmonitor']!=0) {
?>
  document.getElementById('view').visibility="visible";
<?php 
}
?>

function swapmonitor() {
  s='view';
     if (document.all) {
     if (document.all[s]) {
       if (document.all[s].visibility=="visible") {
	 document.cookie = "showmonitor=0";
       	 hide(s);
       } else {
	 document.cookie = "showmonitor=1";
         show(s,'block');
       }
     }
   } else {
     if (document.getElementById(s)) {
       if (document.getElementById(s).visibility=="visible") {
	 document.cookie = "showmonitor=0";
       	 hide(s);
       } else {
	 document.cookie = "showmonitor=1";
         show(s,'block');
       }
     }
   }
}
function showmonitor() {
  s='view';
  if (document.all) {
    if (document.all[s]) {
      document.cookie = "showmonitor=1";
      show(s,'block');
    }
  } else {
    if (document.getElementById(s)) {
      document.cookie = "showmonitor=1";
      show(s,'block');
    }
  }
}
</script>
</div>


<?php

if (!$_SERVER["REMOTE_USER"]) {
  $where["private"]=" AND private=0 ";
 }

  $where["null"]="";
  $order="ORDER BY datec DESC";
    if ($_REQUEST["type"]!=-1) {
      $where["type"]=" AND type='".intval($_REQUEST["type"])."' ";
    }
    if ($_REQUEST["q"]!="") {
      $where["q"]=" AND MATCH(filename,title,description) AGAINST ('".asl($_REQUEST["q"])."') ";
      $order="";
    }

$count=intval($_REQUEST["count"]);
if ($count<=0) $count=100;
$offset=intval($_REQUEST["offset"]);
if ($offset<=0) $offset=0;

list($total)=@mysql_fetch_array(mysql_query("SELECT COUNT(*) FROM media WHERE 1 ".implode($where,"").";"));
if ($total==0 && $_REQUEST["q"]) {
  $where["q"]="  AND ( filename LIKE '%".asl($_REQUEST["q"])."%' OR title LIKE '%".asl($_REQUEST["q"])."%' OR description LIKE '%".asl($_REQUEST["q"])."%' OR id='".asl($_REQUEST["q"])."') ";
  list($total)=@mysql_fetch_array(mysql_query("SELECT COUNT(*) FROM media WHERE 1 ".implode($where,"").";"));
 }

if ($total==0) {
  echo "<div class=\"error\">Aucun résultat ...</div>\n";
 } else {

  $r=mysql_query("SELECT * FROM media WHERE 1 ".implode($where,"")." $order LIMIT $offset,$count;");
  pager($offset,$count,$total,"/?offset=%%offset%%&count=$count&type=".$_REQUEST["type"]."&q=".urlencode($_REQUEST["q"])."&show=".$_REQUEST["show"]."","<p> Media $offset à ".($offset+$count)." - ","</p>");

?>

<table class="formv">
  <tr>
    <th rowspan="2"></th>
    <th rowspan="2" colspan="2">Action</th>
    <th colspan="6">Nom</th>
  </tr>
  <tr>
    <th>Taille</th>
    <th>Durée</th>
    <th>Format</th>
    <th>Audio</th>
    <th>Video</th>
    <th>Date</th>
  </tr>
<?php
$odd="odd";
while ($c=mysql_fetch_array($r)) {
  $tags=mqassoc("SELECT t.id, t.name FROM tag t, mediatag mt WHERE mt.media='".$c["id"]."' AND mt.tag=t.id ORDER BY t.name;");
  /*
  if (($tags[98] || $tags[83]) && (!$_SERVER["REMOTE_USER"])) {
    continue;
  }
  */

  if ($odd=="odd") $odd="even"; else $odd="odd";
  echo "<tr class=\"$odd\">";
  switch ($c["type"]) {
  case MEDIA_UNKNOWN:
    echo "<td rowspan=\"2\"><img id=\"i".$c["id"]."\" src=\"unknown.png\" alt=\"Unknown\" title=\"Unknown\"></td>";
    break;
  case MEDIA_AUDIO:
    echo "<td rowspan=\"2\"><img id=\"i".$c["id"]."\" src=\"audio.png\" alt=\"Audio\" title=\"Audio\"></td>";
    break;
  case MEDIA_IMAGE:
    if (file_exists("formats/15/".$c["id"])) {
      echo "<td rowspan=\"2\"><img src=\"/formats/15/".$c["id"].".jpg\" alt=\"Image\" title=\"Image\" onmouseover=\"show('i".$c["id"]."');\" onmouseout=\"hide('i".$c["id"]."');\"><div id=\"i".$c["id"]."\" class=\"ii\">";
      if (file_exists("formats/8/".$c["id"])) {
	echo "<img src=\"/formats/8/".$c["id"].".jpg\" alt=\"\" title=\"\" />";
      }
      echo "</div></td>";
    } else {
      echo "<td rowspan=\"2\"><img src=\"image.png\" alt=\"Image\" title=\"Image\"></td>";
    }
    break;
  case MEDIA_VIDEO:
    if (file_exists("formats/16/".$c["id"])) {
      echo "<td rowspan=\"2\"><img src=\"/formats/16/".$c["id"]."_small.jpg\" alt=\"Image\" title=\"Image\" onmouseover=\"show('i".$c["id"]."');\" onmouseout=\"hide('i".$c["id"]."');\"><div id=\"i".$c["id"]."\" class=\"ii\">";
      if (file_exists("formats/16/".$c["id"])) {
	echo "<img id=\"v".$c["id"]."\" src=\"/formats/16/".$c["id"].".jpg\" alt=\"\" title=\"\" />";
      }
      echo "</div></td>";
    } else {
      echo "<td rowspan=\"2\"><img id=\"i".$c["id"]."\" src=\"video.png\" alt=\"Vidéo\" title=\"Vidéo\"></td>";
    }
    break;
  }

  echo "<td><a href=\"view.php?id=".$c["id"]."\" target=\"view\" onclick=\"showmonitor();\">Voir</a>&nbsp;";
  echo "(<a href=\"view.php?full=1&id=".$c["id"]."\" target=\"externalmonitor\" alt=\"ouvre le media dans un popup\" title=\"ouvre le media dans un popup\">+</a>)";
  echo "</td>";
  echo "<td><a href=\"get.php?id=".$c["id"]."\">Télécharger</a></td>";


  echo "<td colspan=\"6\">".substr($c["filename"],0,60);
  if (strlen($c["filename"]>60)) echo " ...";
  if ($_REQUEST["show"]>0 && $c["description"]) {
    echo "<br /><small>".nl2br(trim($c["description"]))."</small>";
  }
  if ($_REQUEST["show"]>1) {
    if (count($tags)) {
      echo "<br /><span class=\"taglist\">";
      foreach($tags as $tid=>$tname) {
	echo "<a href=\"index.php?tag=".$tid."\">$tname</a> ";
      }
      echo "</span>";
    }
  }
  echo "</td>";
  echo "</tr><tr class=\"$odd\">";
  if ($_SERVER["REMOTE_USER"]) {
    echo "<td><a href=\"edittc.php?id=".$c["id"]."\">Timecodes</a><br /><a href=\"subtitles.php?id=".$c["id"]."\">Subtitle</a></td>";
    echo "<td><a href=\"edit.php?id=".$c["id"]."\">Editer</a></td>";
  } else {
    echo "<td></td><td></td>";
  }
  echo "<td class=\"details\">".format_size($c["size"])."</td>";
  if ($c["duration"] && $c["type"]!=MEDIA_IMAGE) {
    echo "<td>".sec2date($c["duration"])."</td>";
  } else {
    echo "<td></td class=\"details\">";
  }
  switch ($c["type"]) {
  case MEDIA_AUDIO:
    echo "<td class=\"details\"></td>";
    echo "<td class=\"details\">".$c["acodec"]." ";
    /*
     echo $c["arate"]." ".(($c["channels"]==1)?"Mono":"Stéréo")." ";
     if ($c["abitrate"]) {
     echo $c["abitrate"]."kbps";
     }
    */
    echo "</td>";
    echo "<td class=\"details\"></td>";
    break;

  case MEDIA_IMAGE:
      if ($c["vx"] && $c["vy"]) {
	echo "<td class=\"details\">".$c["vx"]."x".$c["vy"]."</td>";
      } else {
	echo "<td class=\"details\"></td>";
      }
      echo "<td class=\"details\"></td>";
      if ($c["vcodec"]=="mjpeg") $c["vcodec"]="jpeg";
      echo "<td class=\"details\">".$c["vcodec"]."</td>";
      break;
     
    case MEDIA_VIDEO:
      if ($c["vx"] && $c["vy"]) {
	echo "<td class=\"details\">".$c["vx"]."x".$c["vy"]."</td>";
      } else {
	echo "<td class=\"details\"></td>";
      }
      echo "<td class=\"details\">".$c["acodec"]." ";
      /*
      echo $c["arate"]." ".(($c["channels"]==1)?"Mono":"Stéréo")." ";
      if ($c["abitrate"]) {
	echo $c["abitrate"]."kbps";
      }
      */
      echo "</td>";
      echo "<td class=\"details\">".$c["vcodec"]." ";
      if ($c["vfps"]) echo $c["vfps"]."fps ";
      echo "</td>";
      break;
  default:
    echo "<td class=\"details\"></td>";
    echo "<td class=\"details\"></td>";
    echo "<td class=\"details\"></td>";
  }
  echo "<td class=\"details\">".date_my2fr($c["datec"])."</td>";
  echo "</tr>";
 }
 echo "</table>";
 } // found 1 ? ;) 

?>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<?php
require_once("foot.php");
?>