<?php

if (file_exists("bdd.php")) {
  require_once("bdd.php");
 } else {
  mysql_connect("localhost","mk2_dev","poipoi");
  mysql_select_db("mk2_dev");
 }
mysql_query("SET NAMES UTF8;");

require_once("functions.php");

$image_formats=array("png","gif","jpg","bmp","xcf","mjpeg");

define("MEDIA_UNKNOWN",0);
define("MEDIA_AUDIO",1);
define("MEDIA_VIDEO",2);
define("MEDIA_IMAGE",3);

$amedia=array(MEDIA_UNKNOWN=>"Inconnu",
	      MEDIA_AUDIO=>"Audio",
	      MEDIA_VIDEO=>"Vidéo",
	      MEDIA_IMAGE=>"Image",
	      );

$asubetat=array( 0=>"Non Encodée", 1=>"Encodage en cours", 2=>"Encodée", 3=>"Erreurs à l'encodage");
$alang2fr=array( 
		"fr_FR" => "Français", 
		"en_US" => "English", 
		"de_DE" => "Deutsch", 
		"nl_NL" => "Nederlands", 
		"es_ES" => "Español", 
		"ca_ES" => "Català",
		"cs_CS" => "Čeština", 
		"gr_HE" => "Ελληνικά",
		"it_IT" => "Italiano", 
		"da_DA" => "Dansk",
		"ru_RU" => "Русский",
		"pt_PT" => "Português",
		"ja_JP" => "日本語",
		"zh_ZH" => "中文",
		"pl_PL" => "Polski",
		"ar_DZ" => "عربي",
		"bg_BG" => "Български",
		"fi_FI" => "Suomi",
		"sv_SV" => "Svenska",
		"ro_RO" => "Română",
		"tr_TR" => "Türkçe",
		"hu_HU" => "Magyar",
		"hr_HR" => "Hrvatski",
		"lv_LV" => "Latviešu",
		 );

$alang2frsel=array( 
		"fr_FR" => "Français (French)", 
		"en_US" => "English (English)", 
		"de_DE" => "Deutsch (German)", 
		"nl_NL" => "Nederlands (Dutch)", 
		"es_ES" => "Español (Spanish)", 
		"ca_ES" => "Català (Catalan)",
		"cs_CS" => "Čeština (Czeck)", 
		"gr_HE" => "Ελληνικά (Greek)",
		"it_IT" => "Italiano (Italian)", 
		"da_DA" => "Dansk (Danish)",
		"ru_RU" => "Русский (Russian)",
		"pt_PT" => "Português (Portuguese)",
		"ja_JP" => "日本語 (Japanese)",
		"zh_ZH" => "中文 (Chinese)",
		"pl_PL" => "Polski (Polish)",
		"ar_DZ" => "عربي (Arabic)",
		"bg_BG" => "Български (Bulgarian)",
		"fi_FI" => "Suomi (Finish)",
		"sv_SV" => "Svenska (Swedish)",
		"ro_RO" => "Română (Romanian)",
		"tr_TR" => "Türkçe (Turkish)",
		"hu_HU" => "Magyar (Hungarian)",
		"hr_HR" => "Hrvatski (Croatian)",
		"lv_LV" => "Latviešu (Latvian)",
		 );


$privatetags=array(98,83);

?>