<?php

// Compute the big/small sizes : 
    // 1. too much MORE than 16/9 : add black borders on top and bottom
    if ($m["DAR"]>1.82) {
      $aspect="16:9";
    }
    // 2. "almost" proper 16:9 : just remember it's 16:9 
    if ($m["DAR"]<=1.82 && $m["DAR"]>=1.73) { 
      $aspect="16:9";
    }
    // 3. too much LESS than 16:9 will be croped to 4:3 with black borders : 
    if ($m["DAR"]<1.73 && $m["DAR"]>1.37) {
      $aspect="4:3";
    }
    // 4. "almost" proper 4:3  : just remember it's 4:3 
    if ($m["DAR"]<=1.37 && $m["DAR"]>=1.30) {
      $aspect="4:3";
    }
    // 5. too much LESS than 4:3 (strange) will be croped to 4:3 with black border on left/right : 
    if ($m["DAR"]<1.30) {
      $aspect="4:3";
    }

switch($_GET["size"]) {
case "big":
  $what="big";
  if ($aspect=="4:3") {
    if ($m["vx"]>=1000) {
      $bigx="1024"; $bigy="768";
    } else {
      $bigx="640"; $bigy="480";
    }      
  } else {      // 16:9
    if ($m["vx"]>=1000) {
      $bigx="1024"; $bigy="576";
    } else {
      $bigx="640"; $bigy="360";
    }
  }
  break;
case "medium":
  $what="big";
  if ($aspect=="4:3") {
    $bigx="640"; $bigy="480";
  } else {      // 16:9
    $bigx="640"; $bigy="360";
  }
  break;
case "custom":
  $what="small";
  $bigx=intval($_REQUEST["x"]); $bigy=intval($_REQUEST["y"]);
  break;
case "small":
default:
  $what="small";
  if ($aspect=="4:3") {
    $bigx="432"; $bigy="320";
  } else {      // 16:9
    $bigx="480"; $bigy="272";
  }
  break;
}    
$more="";
$poster="/formats/16/".$id.".jpg";

$whatformat="formats";
$vid=$id;
if ($_GET["sub"]) {
  // Search for a sub in this language : 
  $sub=addslashes(substr($_GET["sub"],0,5));
  $mysub=mqone("SELECT * FROM srt WHERE media='$id' AND lang='$sub';");
  if ($mysub["encoded"]==2) {
    $whatformat="formats_srt";
    $vid=$mysub["id"];
    $more=" (".$alang2fr[$mysub["lang"]].")";
  }
}
?>
  <!-- Begin VideoJS -->
<?php if (!isset($_REQUEST["onlyvideo"])) { ?> <h3><?php echo $m["title"].$more; ?></h3> <?php } ?>
  <div class="video-js-box">
    <!-- Using the Video for Everybody Embed Code http://camendesign.com/code/video_for_everybody -->
    <video id="example_video_1" class="video-js" width="<?php echo $bigx; ?>" height="<?php echo $bigy; ?>" controls="controls" preload="none" poster="<?php echo $poster; ?>">
      <source src="/<?php echo $whatformat; ?>/20/<?php echo $vid; ?>_<?php echo $what; ?>.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <source src="/<?php echo $whatformat; ?>/19/<?php echo $vid; ?>_<?php echo $what; ?>.webm" type='video/webm; codecs="vp8, vorbis"' />
<!--       <source src="/<?php echo $whatformat; ?>/18/<?php echo $vid; ?>_<?php echo $what; ?>.ogg" type='video/ogg; codecs="theora, vorbis"' /> -->
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="<?php echo $bigx; ?>" height="<?php echo $bigy; ?>" type="application/x-shockwave-flash"
        data="/flowplayer-3.2.1.swf">
        <param name="movie" value="/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["<?php echo $poster; ?>", {"url": "/<?php echo $whatformat; ?>/20/<?php echo $vid; ?>_<?php echo $what; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="<?php echo $poster; ?>" width="<?php echo $bigx; ?>" height="<?php echo $bigy; ?>" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
    </video>
  </div>
  <!-- End VideoJS -->

  <?php if (!isset($_REQUEST["onlyvideo"])) { ?>
    <div class="description"><?php echo description_filter($m["description"]); ?></div>

    <div class="download">Download : 
      <a href="/<?php echo $whatformat; ?>/20/<?php echo $vid; ?>_<?php echo $what; ?>.mp4">MP4</a>,
      <a href="/<?php echo $whatformat; ?>/19/<?php echo $vid; ?>_<?php echo $what; ?>.webm">WebM</a>,
<!--       <a href="/<?php echo $whatformat; ?>/18/<?php echo $vid; ?>_<?php echo $what; ?>.ogg">Ogg</a> -->
   <?php if (file_exists("formats/21/".$id.".torrent")) { ?>      <a href="/formats/21/<?php echo $id; ?>.torrent">Torrent</a> <?php } ?>
   <?php
   // Is there any subtitle for this video ? 
   $subs=mqlist("SELECT * FROM srt WHERE media='$id';");
$hassubready=false;
if (count($subs)) {
  echo " | Subtitles: ";
  $first=true;
  foreach($subs as $sub) {
    if ($sub["encoded"]==2) {
      $hassubready=true;
    }
    if (!$first) echo ", ";
    echo "<a href=\"/srt/".$sub["id"].".srt\">".$alang2fr[$sub["lang"]]."</a>";
    $first=false;
  }
}
echo "&nbsp; </div>";


if ($hassubready) {
  echo "<div class=\"subs\">";
  echo "View with subtitles in ";
  $first2=true;
  foreach($subs as $sub) {
    if ($sub["encoded"]==2) {
      if (!$first2) echo ", ";
      echo "<a href=\"/embed/$id?";
      $first=true;
      if ($_GET["size"]) { if (!$first) echo "&"; echo "size=".urlencode($_GET["size"]); $first=false; }
      if ($_GET["onlyvideo"]) { if (!$first) echo "&"; echo "onlyvideo=".urlencode($_GET["onlyvideo"]); $first=false; }
      if (!$first) echo "&"; echo "sub=".$sub["lang"];
      echo "\">".$alang2fr[$sub["lang"]]."</a>";
      $first=false;
      $first2=false;
    }
  }
  echo "</div>";
}

} // onlyvideo ?  

