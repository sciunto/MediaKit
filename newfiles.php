<?php

require_once("config.php");

$users=scandir("ftp");

echo "NEWFILES starting scan ".date("d/m/Y H:i:s")."\n"; flush();

foreach($users as $user) {
  echo "USER $user\n"; flush();
  if (substr($user,0,1)==".") 
    continue;
  $d=opendir("ftp/$user");
  while ($c=readdir($d)) {
    if (substr($c,0,1)=="." || 
	!is_file("ftp/$user/$c") ||
	filemtime("ftp/$user/$c")+180>time()
	) 
      continue;
    // Ok, this file is not hidden, is a file, and has a modification time far away.
    // let's add it to the mediakit.
    echo "FILE $c\n"; flush();
    $newname=$c;
    if (file_exists("files/$c")) {
      // Already exists, let's find a newname.
      $base=substr($c,0,strrpos($c,"."));
      $ext=substr($c,strrpos($c,"."));
      $i=1;
      while (file_exists("files/".$base."_".$i.$ext))
	$i++;
      $newname=$base."_".$i.$ext;
    }
    echo "RENAMED TO $newname\n";
    // let's rename it and insert it in the mediakit : 
    rename("ftp/$user/$c","files/$newname");
    $val=getVideoFormat("files/".$newname,1);
    $title=str_replace("_"," ",substr($newname,0,strrpos($newname,".")));
    if ($val["vcodec"]=="") {
      if ($val["acodec"]=="") {
	$mode=MEDIA_UNKNOWN;
      } else {
	$mode=MEDIA_AUDIO;
      }
    } else {
      if ($val["acodec"]=="" && in_array($val["vcodec"],$image_formats)) {
	$mode=MEDIA_IMAGE;
      } else {
	$mode=MEDIA_VIDEO;
      }
    }

    $sql="INSERT INTO media SET filename='".addslashes($newname)."', title='".addslashes($title)."', datec=NOW(), datem=NOW(), type='$mode' ";
    unset($val["filename"]);
    foreach($val as $k=>$v) {
      $sql.=", `$k`='".addslashes($v)."' ";
    }
    mq($sql);
    echo mysql_error();
    echo "SQL:$sql\n"; flush();
    $id=mysql_insert_id();
    $tid=mqonefield("SELECT id FROM tag WHERE groupid='1' AND name='".addslashes($user)."';");
    if ($tid) {
      mq("INSERT INTO tagmedia SET media='$id', tag='$tid';");
    }
  } // while ($c=readdir($d)) {
  closedir($d);
} // foreach $users as $user

echo "NEWFILES ending scan ".date("d/m/Y H:i:s")."\n\n"; flush();

?>