<?php

chdir("..");
require_once("config.php");


$id=intval($_REQUEST["id"]);

if (file_exists("srt/$id")) {
  header("Content-Type: video/subtitle; charset=UTF-8");
  header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
  $me=mqone("SELECT m.title,s.lang FROM media m, srt s WHERE s.media=m.id AND s.id='$id';");
  header('Content-Disposition: attachment; filename="'.str_replace("'",'"',str_replace("/"," ",$me["title"])).'_'.$me["lang"].'.srt"');
  // It will be called downloaded.pdf
  readfile("srt/$id");
}

?>