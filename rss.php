<?php

require_once("config.php");

header("Content-Type: text/xml; charset=utf-8");

echo "<?";
?>xml 
version="1.0" encoding="utf-8"?>
<rss version="2.0" 
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
>

  <channel xml:lang="en">
  <title>Mediakit - La Quadrature du Net<?php if ($title) echo " - ".$title; ?></title>
<link>http://mediakit.laquadrature.net/</link>
<description>Le Mediakit de La Quadrature du Net, Videos, Audio, Images</description>
<language>fr</language>

<?php

if (!isset($_REQUEST["type"])) $_REQUEST["type"]=-1;
if (!isset($_REQUEST["show"])) $_REQUEST["show"]=2;

  $where["null"]="";
  $order="ORDER BY datec DESC";
    if ($_REQUEST["type"]!=-1) {
      $where["type"]=" AND type='".intval($_REQUEST["type"])."' ";
    }
    if ($_REQUEST["q"]!="") {
      $where["q"]=" AND MATCH(filename,title,description) AGAINST ('".asl($_REQUEST["q"])."') ";
      $order="";
    }

$count=20;
$offset=0;

$r=mysql_query("SELECT * FROM media WHERE 1 ".implode($where,"")." $order LIMIT $offset,$count;");

while ($c=mysql_fetch_array($r)) {
  $tags=mqassoc("SELECT t.id, t.name FROM tag t, mediatag mt WHERE mt.media='".$c["id"]."' AND mt.tag=t.id ORDER BY t.name;");
  $date=substr($c["datec"],0,10)."T".substr($c["datec"],11,8)."Z";
  $url=htmlentities("http://mediakit.laquadrature.net/view.php?full=1&id=".$c["id"]);
  $description=iconv("ISO8859-1","UTF-8",xmle($c["description"]));

  echo "<item xml:lang=\"fr\">
<title>".xmle($c["title"])."</title>
<link>$url</link>
<guid isPermaLink=\"true\">$url</guid>
<dc:date>$date</dc:date>
<dc:format>text/html</dc:format>
<dc:language>fr</dc:language>
<dc:creator>La Quadrature du Net</dc:creator>
";
    if (count($tags)) {
      foreach($tags as $tid=>$tname) {
	echo "<dc:subject>".iconv("ISO8859-1","UTF-8",$tname)."</dc:subject>\n";
      }
    }
echo  "
<description>
 $description
</description>
</item>
";
}
?>
</channel>
</rss>


