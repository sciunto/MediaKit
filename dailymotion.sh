#!/bin/sh

function doone_flv {
    read FNAME 
    while [ "$FNAME" ] 
      do 
      NEW="../flv/`echo $FNAME | sed -e 's/\.[^\.]*$/.flv/'`"
      if [ ! -f "$NEW" ]
	  then
	  echo " "
	  echo " "
	  echo "############################################################"
	  echo "##        TRAITEMENT DE $FNAME"
	  echo "############################################################"
	  echo "ffmpeg -i '$FNAME' -acodec libmp3lame -ab 96k -ar 44100 -ac 2 -b 350k -s 480x384  '../flv/`echo $FNAME | sed -e 's/\.[^\.]*$/.flv/'`' "
	  ffmpeg -i "$FNAME" -acodec libmp3lame -ab 96k -ar 44100 -ac 2 -r 25 -b 350k -s 480x384 -y "$NEW"  </dev/null
      fi
      read FNAME 
    done
}

function doone_ogm {
    read FNAME 
    while [ "$FNAME" ] 
      do 
      NEW="../ogm/`echo $FNAME | sed -e 's/\.[^\.]*$/.ogm/'`"
      if [ ! -f "$NEW" ]
	  then
	  ffmpeg2theora "$FNAME"  -x 480 -y 384 -H 44100 -A 96 -V 350 -o "$NEW"  </dev/null
      fi
      read FNAME 
    done
}

cd cut

find -type d -exec mkdir ../flv/{} \;

find -type f | doone_flv

find -type d -exec mkdir ../ogm/{} \;

find -type f | doone_ogm

